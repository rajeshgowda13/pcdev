import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './signup/login/login.component';
import { RegisterComponent } from './signup/register/register.component';
import { EducationalDetailsComponent } from './profile/educational-details/educational-details.component';
import { WorkExpComponent } from './profile/work-exp/work-exp.component';
import { ProfileTypeComponent } from './profile/profile-type/profile-type.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfilePublicComponent } from './profile/profile-public/profile-public.component';
import { BasicInfoComponent } from './profile/basic-info/basic-info.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AuthComponent } from './auth/auth.component';
import { OTPComponent } from './signup/otp/otp.component';
import { LinkedinComponent } from './linkedin/linkedin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BasicInfoComponent,
    LoginComponent,
    RegisterComponent,
    EducationalDetailsComponent,
    WorkExpComponent,
    ProfileTypeComponent,
    ProfileEditComponent,
    ProfilePublicComponent,
    LandingPageComponent,
    AuthComponent,
    OTPComponent,
    LinkedinComponent,
    MyNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
