import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  ngForm: NgForm;

  onChromeLoad() {
    console.log(this.ngForm);
  }
  constructor() { }

  ngOnInit() {
  }

}
