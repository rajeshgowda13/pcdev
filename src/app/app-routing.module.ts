import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './signup/login/login.component';
import { RegisterComponent } from './signup/register/register.component';
import { BasicInfoComponent } from './profile/basic-info/basic-info.component';
import { EducationalDetailsComponent } from './profile/educational-details/educational-details.component';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { ProfilePublicComponent } from './profile/profile-public/profile-public.component';
import { ProfileTypeComponent } from './profile/profile-type/profile-type.component';
import { WorkExpComponent } from './profile/work-exp/work-exp.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AuthComponent } from './auth/auth.component';


const routes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'signin', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path: 'landing', component: LandingPageComponent},
  {
    path: 'profile', component: BasicInfoComponent, children: [
      { path: 'educational-details', component: EducationalDetailsComponent },
      { path: 'edit', component: ProfileEditComponent },
      { path: 'public', component: ProfilePublicComponent },
      { path: 'type', component: ProfileTypeComponent },
      { path: 'work-exp', component: WorkExpComponent }
    ]
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
