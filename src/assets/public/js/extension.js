// Call the extension with a request object
var extensionId = 'ejphmindonnkjnpmpoickmcdibdidjda'

function connectToExtension(request, callback) {
    try {
        chrome.runtime.sendMessage(extensionId, request, function(response) {
            if (response && response.message == "Success") {
                callback(response)
            } else {
                callback(false)
            }
        })
    } catch (err) {
        console.log(err);
        callback(false)
    }
} 