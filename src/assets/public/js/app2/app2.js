var app = angular.module('pcApp', []);
app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })
}])

app.directive('formError', function() {
    return {
        restrict: 'E',
        templateUrl: 'public/partials/error.html'
    }
})

app.directive('datePicker', function() {
    return {
        restrict: 'E',
        templateUrl: '../public/partials/date.html'
    }
})

app.directive('compareTo', function() {
    return {
        require: "ngModel",
        scope: {
            confirmPassword: "=compareTo"
        },
        link: function(scope, element, attributes, modelVal) {
            modelVal.$validators.compareTo = function(val) {
                return val == scope.confirmPassword;
            }

            scope.$watch("confirmPassword", function() {
                modelVal.$validate();
            })
        }
    }
})

app.factory('apiFactory', function($http) {

    var factory = {};

    factory.checkUsername = function(data, callback) {
        const url = '/user/check/' + data;
        $http.get(url).then(function(output) {
            callback(output);
        })
    }

    factory.signUp = function(data, callback) {
        const url = '/user/register'
        $http.post(url, data).then(function(output) {
            callback(output);
        });
    };

    factory.login = function(data, callback) {
        const url = '/user/login';
        $http.post(url, data).then(function(output) {
            callback(output);
        })
    }

    factory.sendOTP = function(callback) {
        const url = '/user/sendOtp';
        $http.post(url).then(function(output) {
            callback(output);
        });
    };

    factory.verifyOtp = function(data, callback) {
        const url = '/user/verifyOtp';
        $http.post(url, data).then(function(output) {
            callback(output);
        });
    };

    factory.resendOTP = function(callback) {
        const url = '/user/resendOtp';
        $http.post(url).then(function(output){
            callback(output);
        });
    };

    factory.updateEmail = function(email, callback) {
        const url = '/user/updateEmail';
        let request = {
            email: email
        }
        $http.post(url, request).then(function(output) {
            callback(output);
        });
    };

    factory.getOrgs = function(callback) {
        const url = '/organization/all';
        $http.get(url).then(function(output) {
            callback(output);
        });
    };

    factory.getSchools = function(orgname, callback) {
        const url = '/organization/search/school/' + orgname;
        $http.get(url).then(function(output) {
            callback(output);
        });
    };

    factory.addRecord = function(record, callback) {
        const url = '/user/record';
        $http.post(url, record).then(function(output) {
            callback(output);
        })
    };

    factory.getSkills = function(callback) {
        const url = '/user/skills/all'
        $http.get(url).then(function(output) {
            callback(output);
        });
    };

    factory.submitSkills = function(skills, callback) {
        const url = '/user/skills/update';
        $http.post(url, skills).then(function(output) {
            callback(output);
        });
    };

    return factory;
})

