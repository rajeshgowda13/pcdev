app.controller('pageFiveCtrl', function($scope, $window, apiFactory) {

    $scope.otpInit = function() {
        
        document.getElementById('resend').style.display = "none";
        console.log(document.referrer)

        //check for Navigation Timing API support
        if (window.performance) {
            console.info("window.performance works fine on this browser");
        } 
        if (performance.navigation.type == 1) {
            console.info( "This page is reloaded" );
            var token = sessionStorage.getItem("token");
            token = JSON.parse(token);
            $scope.email = token.user;
            $scope.setTimer(token); 
        } else {
            console.info( "This page is not reloaded");
            $scope.sendOTP();
        }

        $("#pageLoader").fadeOut();

        // // Check if token already generated, in case the page is refreshed
        // var token = sessionStorage.getItem("token");
        // if (token) {
        //     token = JSON.parse(token);
        //     $scope.email = token.user;

        //     // If timeSince is greater than 90, disable the input and display meessages and resend button, reset the timer
        //     $scope.setTimer(token);            
        // } else {
        //     // No token generated - POST request to generate token
        //     console.log("generating otp");
        //     $scope.sendOTP();
        // }
    }

    $scope.handleServerError = function(response) {
        document.getElementById("server_response").innerHTML = response.data.message;
        $('.response_container').fadeIn();
        setTimeout(function() {
            $('.response_container').fadeOut();
            // document.getElementById("server_response").innerHTML = '';
        }, 5000)
    }

    $scope.handleServerSuccess = function(response) {
        $(".alert-response").removeClass('alert-danger');
        $(".alert-response").addClass('alert-info');
        document.getElementById("server_response").innerHTML = response.data.message;
        $('.response_container').fadeIn();
        setTimeout(function() {
            $('.response_container').fadeOut();
            $window.location.href = '/user/education';
        }, 3000)
    }

    $scope.sendOTP = function () {
        // $scope.failMessage.style.display = "none";
        console.log("Sending OTP")
        apiFactory.sendOTP(function(response) {
            console.log(response);
            if (response.status == 200 && response.data && response.data.status == "Success") {
                console.log(response);
                $scope.email = response.data.user.email;
                // Store variales in session
                let token = {
                    createdAt: Date.now(),
                    user: $scope.email
                }
                sessionStorage.setItem("token", JSON.stringify(token));
                $scope.setTimer(token);
            } else {
                $scope.handleServerError(response);
            }
        })
    }

    $scope.setTimer = function (token) {
        $scope.timer = setInterval(function() {
            let currentTime = Date.now();
            let timeSince = (currentTime - token.createdAt)/1000;

            console.log(timeSince);
            
            if (timeSince > 90) {
                clearInterval($scope.timer);
                document.getElementById("timer").innerHTML = "OTP expired";
                document.getElementById("resend").style.display = "block";
            } else {
                document.getElementById("resend").style.display = "none";
                document.getElementById("timer").innerHTML = Math.ceil(90 - timeSince) + " seconds left";
            }
        }, 1000)
    }

    $scope.verifyOtp = function() {

        var otp = document.getElementById("otp").value

        if (otp.length != 6) {
            console.log("Invalid Otp or timer expired");
            return
        }

        // If timer running and otp is proper length - send otp

        let request = {
            email: $scope.email,
            otp: otp
        }
        apiFactory.verifyOtp(request, function(response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                $scope.fail = false;
                clearInterval($scope.timer);
                sessionStorage.removeItem("token");
                document.getElementById("timer").style.display = "none";
                $scope.handleServerSuccess(response);
                
            } else {
                $scope.handleServerError(response);
            }
        })
    }

    $scope.updateEmail = function() {
        apiFactory.updateEmail($scope.email, function(response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                clearInterval($scope.timer);
                let token = {
                    createdAt: Date.now(),
                    user: $scope.email
                }
                sessionStorage.setItem("token", JSON.stringify(token));
                $scope.setTimer(token);
                $("#sample").modal('hide');
            } else {
                $scope.handleServerError(response);
            }
        })
    }

    $scope.cancelEmailUpdate = function() {
        $scope.email = JSON.parse(sessionStorage.getItem("token")).user;
        $("#sample").modal('hide');
    }
    
})