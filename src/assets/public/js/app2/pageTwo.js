app.controller('pageTwoCtrl', function($scope, $window) {

    $scope.progress = null;

    $scope.isProgress = function() {
        $scope.progress = JSON.parse(sessionStorage.getItem('progress'));
        // if (!$scope.progress) {
        //     $window.location = '/';
        // }
    }

    $scope.assignType = function(type) {
        sessionStorage.setItem('usertype', type);
        $scope.progress['usertype'] = type;
        $scope.progress.step = 2;
        sessionStorage.setItem('progress', JSON.stringify($scope.progress));
        $window.location = '/linkedin';
    }

    $scope.handleBack = function() {
        $window.location.href = '/signup';
    }
})