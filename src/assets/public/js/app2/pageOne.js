app.controller('pageOneCtrl', function($scope, $window, apiFactory) {

    $("#pageLoader").fadeOut();

    /* Function to check if username available and show proper feedback */
    $scope.checkUsername = function () {

        $scope.error = false;
        $scope.formMessage = '';
        $scope.success = false;
        document.getElementById('feedback').className = '';

        if ($scope.signupform.username.$error.minlength && !$scope.signupform.username.$pristine) {
            document.getElementById('feedback').classList.add('failure-feedback');
            $scope.error = true;
            $scope.formMessage = 'Username must be 4 characters.';
            return;
        }

        if ($scope.signupform.$valid) {
            apiFactory.checkUsername($scope.username, function(response) {
                if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                    $scope.formMessage = response.data.message;
                    if (response.data.message == 'Available') {
                        $scope.error = false;
                        document.getElementById('feedback').classList.add('success-feedback');
                        $scope.success = true;
                    } else {
                        $scope.success = false;
                        document.getElementById('feedback').classList.add('failure-feedback');
                        $scope.error = true;
                    }
                } else {
                    $scope.success = false;
                    document.getElementById('feedback').classList.add('failure-feedback');
                    $scope.error = true;
                    $scope.formMessage = 'Invalid input';
                }
            })
        }

        return; 
    }

    /* Function to submit username and continue with signup */
    $scope.setUsername = function(isValid) {
        if (isValid) {
            sessionStorage.setItem('username', $scope.username);
            let progress = {
                username: $scope.username,
                step: 1
            }
            sessionStorage.setItem('progress', JSON.stringify(progress));
            $window.location.href = '/usertype';
        }
    }

    /* Listener for login from plugin */
    $window.addEventListener('message', function(event) {

        if (event.source !== $window) return;

        if (event.data.type == "initExisting") {
            console.log(event.data.text);
            apiFactory.login(event.data.text, function(response) {
                if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                    setTimeout(function() {
                        $window.location.href = '/user/profile';
                    },2000);
                }
            })
        }
    })
})