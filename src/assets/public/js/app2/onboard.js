const domain = "http://app.peoplechain.test";

app.controller('recordCtrl', function($scope, $window, $location, apiFactory) {

    $scope.maxDate = new Date('2023', '06');
    $scope.minDate = new Date('1970', '01');

    let usertype = sessionStorage.getItem("usertype");
    console.log("User type" + usertype)

    if (usertype == "student") {
        let skip_btn = document.getElementById("skipStep");
        if (skip_btn) {
            skip_btn.style.display = "block";
        }
    }

    $scope.skipStep = function () {
        $window.location.href = '/user/skills';
    }

    $scope.addRecord = function (isValid, type) {
        console.log(isValid)
        if (isValid) {
            var record;
            //from date
            var fromDate = document.getElementById("startdate").value.trim();
            //to date
            var toDate = document.getElementById("enddate").value.trim();
            var startdate, enddate, from, to;
            console.log(fromDate,toDate);

            if (fromDate == "" && toDate == "") {
                //console.log("yes");
                var span = document.getElementById("error_from");
                span.innerHTML = "Please select dates";
                document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            }
            else if (fromDate == "" && toDate != null) {
                var span = document.getElementById("error_from");
                span.innerHTML = "Please select from date";
                document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            }
            else if (toDate == "" && fromDate != "") {
                if (fromDate.length != 7) {
                    var span = document.getElementById("error_from");
                    span.innerHTML = "Invalid from date";
                    document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                } else {
                    enddate = "Present";
                    var splittedFrom = fromDate.split("/");
                    from = splittedFrom[0] + "/01/" + splittedFrom[1];
                    startdate = new Date(from);

                    let orgName = document.getElementById("cname").value;
                    record = {
                        orgName: orgName,
                        from: startdate,
                        to: enddate,
                        role: ($scope.role) ? $scope.role : $scope.ctype + " of " + $scope.cspecial,
                        department: $scope.department ? $scope.department : "Null",
                        location: ($scope.location) ? $scope.location : "Null",
                        record_type: $scope.ctype2,
                        type: type.toString()
                    }
                    add(record);
                }
            } else {
                if (fromDate.length != 7) {
                    var span = document.getElementById("error_from");
                    span.innerHTML = "Invalid from date";
                    document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else if (toDate.length != 7) {
                    var span = document.getElementById("error_from");
                    span.innerHTML = "Invalid to date";
                    document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                } else if (toDate.length != 7 && fromDate.length != 7) {
                    var span = document.getElementById("error_from");
                    span.innerHTML = "Invalid dates";
                    document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                } else {
                    var splittedFrom = fromDate.split("/");
                    from = splittedFrom[0] + "/01/" + splittedFrom[1];

                    var splittedTo = toDate.split("/");
                    to = splittedTo[0] + "/01/" + splittedTo[1];

                    startdate = new Date(from);
                    enddate = new Date(to);

                    let orgName = document.getElementById("cname").value;

                    record = {
                        orgName: orgName,
                        from: startdate,
                        to: (enddate) ? enddate : "Present",
                        role: ($scope.role) ? $scope.role : $scope.ctype + " of " + $scope.cspecial,
                        department: $scope.department ? $scope.department : "Null",
                        location: ($scope.location) ? $scope.location : "Null",
                        record_type: $scope.ctype2,
                        type: type.toString()
                    }
                    add(record);
                }
            }
            function add(record) {
                document.getElementById("loader").style.display = "block";
                console.log(record)
                var currentDate = new Date();

                if (record.to != "Present") {
                    //compare 2 dates
                    if (record.from > record.to) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "From date can not be greater than to date";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    } else if (record.from < new Date("01/01/1970")) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "Invalid from date, select date after 1970";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    }
                    else if (record.to > new Date("12/01/2023")) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "To date can not cross 2023";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    }
                    else if (record.from > currentDate) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "From can not be future date, kindly select valid one";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    } else {
                        console.log("Success");
                        document.getElementById("error_from").style.display = "none";
                        apiFactory.addRecord(record, function (response) {
                            if (response.status == 200 && response.data && response.data.status == "Success") {
                                setTimeout(function () {
                                    document.getElementById("success").style.display = "block";
                                    $scope.successMessage = response.data.message;
                                    setTimeout(function () {
                                        // Get current location - if professional - redirect to skills
                                        if ($location.path().split('/').pop() == 'professional') {
                                            $window.location.href = '/user/skills'
                                        } else {
                                            $window.location.href = '/user/professional'
                                        }
                                    }, 1000);
                                }, 1000);
                            } else {
                                document.getElementById("loader").style.display = "none";
                                $scope.fail = true;
                                $scope.failMessage = response.data.message;
                            }
                        })
                    }
                } else {
                    if (record.from > currentDate) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "From can not be future date, kindly select valid one";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    } else if (record.from < new Date("01/01/1970")) {
                        var span = document.getElementById("error_from");
                        span.innerHTML = "Invalid from date, select date after 1970";
                        document.getElementById("loader").style.display = "none";
                        span.style.display = "block";
                    }
                    else {
                        console.log("success");
                        document.getElementById("error_from").style.display = "none";
                        apiFactory.addRecord(record, function (response) {
                            if (response.status == 200 && response.data && response.data.status == "Success") {
                                setTimeout(function () {
                                    document.getElementById("success").style.display = "block";
                                    $scope.successMessage = response.data.message;
                                    setTimeout(function () {
                                        // Get current location - if professional - redirect to skills
                                        if ($location.path().split('/').pop() == 'professional') {
                                            $window.location.href = '/user/skills'
                                        } else {
                                            $window.location.href = '/user/professional'
                                        }
                                    }, 1000);
                                }, 1000);
                            } else {
                                document.getElementById("loader").style.display = "none";
                                $scope.fail = true;
                                $scope.failMessage = response.data.message;
                            }
                        })
                    }
                }
            }
        }
    }

    $(".organization-data-ajax").select2({
        ajax: {
            url: domain + "/organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'company',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    function formatOrg (org) {
        if (org.loading) {
            return org.text;
        }

        var markup =  '<span style="width: 20%;float: left"><img style="width: 20px;position: relative;top: 8px;margin-right: 10px;" src="' + org.logoURL + '" /></span>' +
        '<span style="width: 60%;float: left;">' + org.orgname + ' <br> <small style="position: relative;left: 0px;">' + org.location + '</small></span>' 

        if (org.verified) {
            markup +=  '<span style="width: 20%;float: left;text-align:right"><i class="fas fa-check-circle"></i></span>'
        } else {
            markup += '<span style="width: 20%;float: left;text-align:right;margin-top:10px;"><i class="fas fa-times-circle"></i></span>'
        }

        return markup;
    }

    function formatOrgSelection (org) {
        return org.orgname || org.text;
    }
})

app.controller('skillCtrl', function ($scope, $window, apiFactory) {
    $scope.skills = [];
    $scope.mySkills = [];
    $scope.skillError = false;
    $scope.fail = false;
    $scope.getSkills = function () {
        apiFactory.getSkills(function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                response.data.skills.forEach((skill) => {
                    $scope.skills.push(skill.skill);
                })
                autocomplete(document.getElementById('skills'), $scope.skills);
                $("#pageLoader").fadeOut();
            } else {
                $("#pageLoader").fadeOut();
                document.getElementById("server_response").innerHTML = response.data.message;
                $('.response_container').fadeIn();
                setTimeout(function() {
                    $('.response_container').fadeOut();
                    document.getElementById("server_response").innerHTML = response.data.message;
                }, 5000)
            }
        })
    }


    // $scope.addMySkill = function() {
    //     let skill = document.getElementById('skills').value;
    //     if ($scope.skills.includes(skill) && $scope.mySkills.length < 6 && !$scope.mySkills.includes(skill)) {
    //         $scope.mySkills.push(skill)
    //         document.getElementById('skills').value = '';
    // if ($scope.mySkills.length == 5) {
    //     $scope.skillError = true;
    //     $scope.skillErrorMessage = "You can only add upto 5 skills, please edit below to add more.";
    // }
    //     } else if ($scope.mySkills.includes(skill)) {
    //         document.getElementById('skills').value = '';
    //     }
    // }

    $("#skills").on('change', function (e) {
        var skill = document.getElementById("skills").value;
        console.log("walah" + skill);
        if ($scope.skills.includes(skill) && $scope.mySkills.length < 6 && !$scope.mySkills.includes(skill)) {
            if ($scope.mySkills.length == 5) {
                $scope.skillError = true;
                $scope.skillErrorMessage = "You can only add upto 5 skills, please edit below to add more.";
            }
            $scope.$apply(function () {
                $scope.mySkills.push(skill);
            })
            document.getElementById('skills').value = '';
        } else {
            console.log("some issue");
        }

        // $scope.newSkills.push(skill);
        // $scope.mySkills.push(skill);
        // console.log($scope.newSkills);
        // console.log($scope.mySkills);
        // document.getElementById("skill").value = '';
    })

    $scope.deleteMySkill = function (skill) {
        var index = $scope.mySkills.indexOf(skill);
        if (index > -1) {
            $scope.mySkills.splice(index, 1);
            $scope.skillError = false;
        }
    }

    $scope.submitSkills = function () {
        document.getElementById("loader").style.display = "block";
        let request = {
            skills: $scope.mySkills
        }
        apiFactory.submitSkills(request, function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                // document.getElementById("success").style.display = "block";
                    // $scope.successMessage = response.data.message;
                    document.getElementById("loader").style.display = "none";
                    $(".alert-response").removeClass('alert-danger');
                    $(".alert-response").addClass('alert-info');
                    document.getElementById("server_response").innerHTML = response.data.message;
                    $('.response_container').fadeIn();
                    setTimeout(function() {
                        $('.response_container').fadeOut();
                        $window.location.href = '/user/profile';
                    }, 3000)
            } else {
                document.getElementById("loader").style.display = "none";
                document.getElementById("server_response").innerHTML = response.data.message;
                $('.response_container').fadeIn();
                setTimeout(function() {
                    $('.response_container').fadeOut();
                    document.getElementById("server_response").innerHTML = response.data.message;
                }, 5000)
            }
        })
    }
})