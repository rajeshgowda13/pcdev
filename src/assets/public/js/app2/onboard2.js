const domain = "http://app.peoplechain.test";

app.controller('educationCtrl', function($scope, $window, $location, apiFactory) {

    $scope.record_type = "";

    $(".school-data-ajax").select2({
        ajax: {
            url: domain + "/organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'school',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    $(".college-data-ajax").select2({
        ajax: {
            url: domain + "/organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'college',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    function formatOrg (org) {
        if (org.loading) {
            return org.text;
        }

        var markup =  '<span style="width: 20%;float: left"><img style="width: 20px;position: relative;top: 8px;margin-right: 10px;" src="' + org.logoURL + '" /></span>' +
        '<span style="width: 60%;float: left;">' + org.orgname + ' <br> <small style="position: relative;left: 0px;">' + org.location + '</small></span>' 

        if (org.verified) {
            markup +=  '<span style="width: 20%;float: left;text-align:right"><i class="fas fa-check-circle"></i></span>'
        } else {
            markup += '<span style="width: 20%;float: left;text-align:right;margin-top:10px;"><i class="fas fa-times-circle"></i></span>'
        }

        return markup;
    }

    function formatOrgSelection (org) {
        return org.orgname || org.text;
    }

    // $scope.assignRecordType(target) {
    //     console.log(target + "Hello");
    // }
    $('a').on('click', function () {
        var target = $(this).attr('rel');

        console.log(target, 'asdsad');
        $('.profile_type_block').hide();
        $('#pt_info').show();
        console.log($scope.record_type, 'scope record type');

        $('#' + target).show(500).siblings(".pt_info_block").hide(500);
        // $('#' + target).show(500).siblings(".banner_change").hide(500);
        if (target === 'school') {
            console.log('school');
            $scope.record_type = "3";
            document.getElementById('school1').style.display = 'block';
            document.getElementById('banner_change').style.display = 'none';
            document.getElementById('title').innerHTML = 'Add your School Credential';
            document.getElementById('diploma1').style.display = 'none';
        }
        if (target === 'diploma') {
            console.log('diploma');
            $scope.record_type = "4";
            document.getElementById('diploma1').style.display = 'block';
            document.getElementById('banner_change').style.display = 'none';
            document.getElementById('school1').style.display = 'none';
            document.getElementById('title').innerHTML = 'Add your Under Graduate Credential';
        }
        if (target === 'ug' || target === 'pg' || target === 'doctrate' || target === 'doctrate' || target === 'other') {
            $scope.record_type = "5";
            document.getElementById('diploma1').style.display = 'block';
            document.getElementById('banner_change').style.display = 'none';
            document.getElementById('school1').style.display = 'none';
            document.getElementById('title').innerHTML = 'Add your Under Graduate Credential';
        }
    });

    $scope.addRecord = function() {
        console.log("Add record");

        if ($scope.record_type == "") {
            return
        }

        var record = {};
        var span = document.getElementById("error_from_work");
        
        // Check if school or diploma/uni etc
        
        if ($scope.record_type == "3") {
            // No joining date
            var passDate = $scope.passyear;
            var span = document.getElementById("error_from_work");
            let orgname = document.getElementById("cname").value;
        
            if (passDate == "") {
                span.innerHTML = "Please select a valid graduation date";
                span.style.display = "block";
                return
            }
        
            passDate = new Date(passDate);
        
            record = {
                orgName: orgname,
                from: null,
                to: passDate,
                role: $scope.sboard,
                department: $scope.grade,
                location: "",
                type: $scope.record_type,
                record_type: "fulltime"
            }
        
        } else {
            var joinYear = $scope.jyear;
            var passYear = $scope.upyear;

            if (joinYear == "") {
                span.innerHTML = "Please select a start date";
                return
            }

            if (passYear == "" || passYear == null) {
                passYear = "Present";
            } else {
                passYear = new Date(passYear)
            }

            joinYear = new Date(joinYear);
            let currentDate = new Date();

            if (currentDate < joinYear) {
                span.innerHTML = "Start date cannot be in the future";
                span.style.display = "block";
                return
            }

            if (passYear != "Present" && passYear < joinYear) {
                span.innerHTML = "Start date cannot be greater than end date";
                span.style.display = "block";
                return
            }

            let orgname = document.getElementById("uname").value;

            record = {
                orgName: orgname,
                from: joinYear,
                to: passYear,
                role: $scope.ucourse,
                department: $scope.specialization,
                location: "",
                type: $scope.record_type,
                record_type: $scope.ctype 
            }
        }

        console.log(record);

        apiFactory.addRecord(record, function(response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                $window.location.href = '/user/professional'
            } else {
                console.log("Unable to add record")
            }
        })
    }
})