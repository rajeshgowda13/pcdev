app.controller('signInCtrl', function($scope, $window, $location, apiFactory) {

    console.log($location.search());

    // If search parameter present - redirect to that value else to profile
    $scope.redirectUrl = $location.search()['redirectUrl'];
    if ($location.search() && $scope.redirectUrl !== undefined) {
        $scope.redirectUrl = $location.search()['redirectUrl'];
        $scope.urlObject = $location.search();
        for (let key in $scope.urlObject) {
            if (key != 'redirectUrl') {
                $scope.redirectUrl += "&" + key + "=" + $scope.urlObject[key];
            }
        }
    } else {
        $scope.redirectUrl = '/user/profile';
    }

    console.log($scope.redirectUrl);
    $scope.serverError = false;
    var i = sessionStorage.length;
    while(i--) {
        var key = sessionStorage.key(i);
        sessionStorage.removeItem(key); 
    }

    $("#pageLoader").fadeOut();

    $scope.handleServerError = function(response) {
        document.getElementById("server_response").innerHTML = response.data.message;
        $('.response_container').fadeIn();
        setTimeout(function() {
            $('.response_container').fadeOut();
            // document.getElementById("server_response").innerHTML = '';
        }, 5000)
    }

    $scope.handleServerSuccess = function(response) {
        $(".alert-response").removeClass('alert-danger');
        $(".alert-response").addClass('alert-info');
        document.getElementById("server_response").innerHTML = response.data.message;
        $('.response_container').fadeIn();
        setTimeout(function() {
            $('.response_container').fadeOut();
            $window.location.href = '/user/education';
        }, 3000)
    }
    
    $scope.login = function (isValid) {
        if (isValid) {

            let username = document.getElementById("username").value;
            let password = document.getElementById("password").value;

            let request = {
                username: username,
                password: password
            }

            apiFactory.login(request, function(response) {
                console.log(response);
                if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                    $window.location.href = $scope.redirectUrl;
                } else if (response.data.status == "Unverified") {
                    setTimeout(function() {
                        $window.location.href = '/user/verifyOtp';
                    }, 2000);
                } else {
                    $scope.handleServerError(response);
                }
            })

        }
    }
})