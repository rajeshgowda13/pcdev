app.controller('linkedinCtrl', function($scope, $window) {

    $scope.progress = null;

    $scope.isProgress = function() {
        $scope.progress = JSON.parse(sessionStorage.getItem('progress'));
        console.log($scope.progress)
        // if (!$scope.progress) {
        //     $window.location = '/';
        // }
    }

    $scope.handleBack = function() {
        $window.location.href = '/usertype';
    }

    $scope.linkedinAuth = function() {
        $scope.progress['linkedin'] = true;
        $scope.progress.step = 3;
        sessionStorage.setItem('progress', JSON.stringify($scope.progress));
        $window.location.href = '/linkedin/auth';
    }

    $scope.skipAuth = function() {
        $scope.progress['linkedin'] = false;
        $scope.progress.step = 3;
        sessionStorage.setItem('progress', JSON.stringify($scope.progress));
        $window.location.href = '/basicInfo';
    }
})