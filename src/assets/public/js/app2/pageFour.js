app.controller('pageFourCtrl', function($scope, $window,$location, apiFactory) {

    $scope.progress = null;
    $scope.phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;

    $scope.checkLinkedIn = function() {

        $scope.progress = JSON.parse(sessionStorage.getItem('progress'));
        console.log($scope.progress)
        // if (!$scope.progress) {
        //     $window.location = '/';
        // }

        if ($scope.progress.registered) {
            alert("Form Resubmission: You are already registered, please login.")
            setTimeout(function() {
                $window.location = '/signin'
            }, 2000);
        }

        if ($location.search() && $location.search()._raw) {
            $scope.linked_data = JSON.parse($location.search()._raw);
            $scope.fname = $scope.linked_data.firstName;
            $scope.lname = $scope.linked_data.lastName;
            $scope.email = $scope.linked_data.emailAddress;
            sessionStorage.setItem('linkedin', $scope.linked_data)
        }
    }

    $("#pageLoader").fadeOut();

    $scope.signUp = function(isValid) {
        if (isValid) {
            var data = {
                username: $scope.progress.username,
                type: $scope.progress.usertype,
                password: $scope.password,
                email: $scope.email,
                profile: {
                    fname: $scope.fname,
                    lname: $scope.lname,
                    mobile: $scope.mobile.toString(),
                    displayPic: $scope.linked_data ? $scope.linked_data.pictureUrls.values[0] : null
                }
            }

            apiFactory.signUp(data, function(response) {
                console.log(response);
                if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {

                    $scope.progress['registered'] = true;
                    $scope.progress['email'] = $scope.email;
                    $scope.progress.step = 4;
                    sessionStorage.setItem('progress', JSON.stringify($scope.progress));

                    sessionStorage.setItem('email', $scope.email)
                    $(".alert-response").removeClass('alert-danger');
                    $(".alert-response").addClass('alert-info');
                    document.getElementById("server_response").innerHTML = response.data.message;
                    $('.response_container').fadeIn();
                    setTimeout(function() {
                        $('.response_container').fadeOut();
                        $window.location.href = '/user/verifyOtp'
                    }, 3000)
                } else {
                    // $scope.serverError = true;
                    // $scope.message = response.data.message;
                    document.getElementById("server_response").innerHTML = response.data.message;
                    $('.response_container').fadeIn();
                    setTimeout(function() {
                        $('.response_container').fadeOut();
                        document.getElementById("server_response").innerHTML = response.data.message;
                    }, 5000)
                } 
            });

        }
        return false;
    }

    $scope.handleBack = function() {
        $window.location.href = '/linkedin';
    }


})