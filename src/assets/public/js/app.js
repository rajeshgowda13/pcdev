const domain = "http://app.peoplechain.test/";

var app = angular.module('pcApp', []);
app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })
}])

app.directive("selectNgFiles", function () {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var file = elem[0].files[0];
                ngModel.$setViewValue(file);
            })
        }
    }
});

app.directive("fileModel", ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
    
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])

app.controller('profileController', function($scope, $window, dataFactory) {

    console.log(document.referrer);
    document.getElementById("secretKey_create").value = null;
    document.getElementById("detecting").style.display = "none";
    document.getElementById("installed").style.display = "none";

    $scope.maxDate = new Date('2023', '06');
    $scope.minDate = new Date('1970', '01');
    $scope.extensionInstalled = false;

    $scope.otpSend = sessionStorage.getItem('otpSend') ? sessionStorage.getItem('otpSend') : false;
    $scope.organizations = [];
    $scope.skills = [];
    $scope.mySkills = [];
    $scope.newSkills = [];
    $scope.deletedSkills = [];
    $scope.getUser = function () {

        let request = {
            method: "connectExt"
        }

        connectToExtension(request, (result) => {
            if (result) {
                $scope.extensionInstalled = true;
            } else {
                $scope.extensionInstalled = false;
            }
        })

        dataFactory.getUser(function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                console.log(response)
                $scope.user = response.data.user;
                $scope.profilePic = ($scope.user.profile.avatar) ? (domain + $scope.user.profile.avatar): $scope.user.profile.displayPic;
                $scope.user_location = ($scope.user.profile.location) ? $scope.user.profile.location : '';

                // response.data.organizations.forEach((organization) => {
                //     $scope.organizations.push(organization.orgname);
                // });
                response.data.skills.forEach((skill) => {
                    $scope.skills.push(skill.skill);
                })

                $scope.endorsedSkills = [];
                $scope.mySkills = response.data.user.skills;
                $scope.endorsements = response.data.user.endorsements;

                // Skills and endrosements

                $scope.mySkills.forEach((skill) => {
                    let temp = {};
                    temp.skill = skill;
                    temp.count = 0;
                    $scope.endorsements.forEach((endorsements) => {
                        if (skill == endorsements.skill) {
                            temp.endorsements = endorsements.endorsements;
                            temp.count = endorsements.endorsements.length;
                        }
                    })

                    $scope.endorsedSkills.push(temp);
                })

                console.log($scope.endorsedSkills + " Hurray")


                $scope.viewEndorsement = function(endorsements) {
                    $scope.modalEndorsements = endorsements;
                    $('#skillEndorseModal').modal('show');
                }


                // autocomplete(document.getElementById('cname'), $scope.organizations);
                autocomplete(document.getElementById('wcompany'), $scope.organizations);
                autocomplete(document.getElementById('skill'), $scope.skills);
                $scope.education = [];
                $scope.work = [];
                if ($scope.user.address) {
                    let qr_text = domain + $scope.user.username;
                    new QRCode(document.getElementById("qrcode"), qr_text);
                }
                if ($scope.user.records.length > 0) {
                    $scope.user.records.forEach((record) => {

                        if (record.type == "3" || record.type == "4" || record.type == "5") {
                            record.ended = new Date(record.endDate).toLocaleDateString().split('/')[2];
                            $scope.education.push(record);
                        } else if (record.type == "2") {
                            record.started = new Date(record.startDate).toLocaleDateString();
                            record.ended = new Date(record.endDate).toLocaleDateString();
                            console.log(record.endDate);
                            if (record.ended == '01/01/1970') {
                                record.ended = "Present"
                            }
                            $scope.work.push(record);
                        }
                    })
                }
                // Remove the loader
                // document.getElementById("pageLoader").style.display = "none";
                $("#pageLoader").fadeOut();

                // $(".js-example-data-ajax").select2({
                //     dropdownParent: $('#addeducationalInfo'),
                //     ajax: {
                //         url: domain + "/organization/search",
                //         dataType: 'json',
                //         delay: 250,
                //         data: function (params) {
                //             return {
                //                 orgname: params.term, //search term
                //                 orgtype: 0,
                //                 page: params.page
                //             };
                //         },
                //         processResults: function (data, params) {
                //             console.log(data, "I am here");
                //             params.page = params.page || 1;

                //             var i = 0;
                //             while (i < data.organizations.length) {
                //                 console.log(data.organizations[i])
                //                 data.organizations[i]["id"] = data.organizations[i]["_id"];
                //                 i++;
                //             }

                //             return {
                //                 results: data.organizations,
                //                 pagination: {
                //                     more: (params.page * 30) < data.total_count
                //                 }
                //             };
                //         },
                //         cache: true
                //     },
                //     placeholder: 'Search for an Organization',
                //     escapeMarkup: function (markup) {return markup;},
                //     minimumInputLength: 1,
                //     templateResult: formatOrg,
                //     templateSelection: formatOrgSelection
                // });

                // function formatOrg (org) {
                //     if (org.loading) {
                //         return org.text;
                //     }

                //     var markup =  '<span style="width: 20%;float: left"><img style="width: 20px;position: relative;top: 8px;margin-right: 10px;" src="' + org.logoURL + '" /></span>' +
                //     '<span style="width: 60%;float: left;">' + org.orgname + ' <br> <small style="position: relative;left: 0px;">' + org.location + '</small></span>' 

                //     if (org.verified) {
                //         markup +=  '<span style="width: 20%;float: left;text-align:right;margin-top:10px;color:green;"><i class="fas fa-check-circle"></i></span>'
                //     } else {
                //         markup += '<span style="width: 20%;float: left;text-align:right;margin-top:10px;color:red;"><i class="fas fa-times-circle"></i></span>'
                //     }

                //     return markup;
                // }

                // function formatOrgSelection (org) {
                //     return org.orgname || org.text;
                // }

                if ($scope.user.address) {
                    console.log("Getting user")
                    let request = {
                        method: "getUser",
                        user: $scope.user.address
                    }
                    connectToExtension(request, (result) => {
                        console.log("Getting user result: " + JSON.stringify(result));
                        if (result && result.account) {
                            sessionStorage.setItem("currentUser", JSON.stringify(result.account));
                            console.log("User account set in session");
                        } else if (result && !result.account) {
                            let local = sessionStorage.getItem("currentUser");
                            if (!local) {
                                console.log("Account not found");
                                $("#createCryptoAccount").modal('show');
                            } else {
                                console.log("Account found locally");
                                let local_json = JSON.parse(local);
                                if (local_json.username == $scope.user.username && local_json.publicKey == $scope.user.address) {
                                    console.log("Awesome, your session was already set");
                                } else {
                                    // Clear the session and ask user to login to plugin
                                    sessionStorage.removeItem("currentUser");
                                    $("#createCryptoAccount").modal('show');
                                }
                            }
                        } else {
                            // Show banner to install plugin
                            console.log("Plugin not installed");
                            document.getElementById('banner').style.display = "block";
                        }
                    })
                } else {
                    $scope.user.address = null;
                }
                if (!$scope.user.isPluginEducated && $scope.user.address && $scope.extensionInstalled) {
                    // Review Modal
                    console.log("User will be educated");
                    $("#reviewModal").modal({
                        backdrop: "static",
                        keyboard: false
                    });
                }

                if (!$scope.user.isProfileIntroduced) {
                    $("#pcWelcomeModal").modal({
                        backdrop: "static",
                        keyboard: false
                    })
                }

                if (window.location.href.indexOf('#chromeModal') != -1) {
                    $('#chromeModal').modal('show');
                }
            } else {
                console.log(response);
                console.log("Could not find user");
            }
        })
    }

    $(".organization-data-ajax").select2({
        dropdownParent: $("#addworkexpInfo"),
        ajax: {
            url: domain + "organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'company',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    $(".school-data-ajax").select2({
        dropdownParent: $("#addeducationalInfo"),
        ajax: {
            url: domain + "organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'school',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    $(".college-data-ajax").select2({
        dropdownParent: $("#addeducationalInfo"),
        ajax: {
            url: domain + "organization/search",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    orgtype: 'college',
                    orgname: params.term, //search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                console.log(data, "I am here");
                params.page = params.page || 1;

                var i = 0;
                while (i < data.organizations.length) {
                    console.log(data.organizations[i])
                    data.organizations[i]["id"] = data.organizations[i]["_id"];
                    i++;
                }

                return {
                    results: data.organizations,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        placeholder: 'Search for an Organization',
        escapeMarkup: function (markup) {return markup;},
        minimumInputLength: 1,
        templateResult: formatOrg,
        templateSelection: formatOrgSelection
    });

    function formatOrg (org) {
        if (org.loading) {
            return org.text;
        }

        var markup =  '<span style="width: 20%;float: left"><img style="width: 20px;position: relative;top: 8px;margin-right: 10px;" src="' + org.logoURL + '" /></span>' +
        '<span style="width: 60%;float: left;">' + org.orgname + ' <br> <small style="position: relative;left: 0px;">' + org.location + '</small></span>' 

        if (org.verified) {
            markup +=  '<span style="width: 20%;float: left;text-align:right"><i class="fas fa-check-circle"></i></span>'
        } else {
            markup += '<span style="width: 20%;float: left;text-align:right;margin-top:10px;"><i class="fas fa-times-circle"></i></span>'
        }

        return markup;
    }

    function formatOrgSelection (org) {
        return org.orgname || org.text;
    }

    $scope.startTour = function () {
        $("#pcWelcomeModal").modal('hide');
        console.log("Tour initiated");
        var intro = introJs();

        intro.setOptions({
            exitOnOverlayClick: false,
            steps: [{
                element: '#step1',
                intro: "<b>Basic Inforamation</b> <br> You can view or edit your basic profile details from here. Don’t forget to verify your Mobile Number post this tour"
            }, {
                element: '#step2',
                intro: "<b>Academic Qualifications</b> <br> <p>All your academic qualifications such as this one are displayed here when added.</p>",
                position: 'right'
            }, {
                element: '#step3',
                intro: '<b>Verify your Academic Qualifications</b> <br> <p>You can click here to add a suitable supporting document to get your CGPA verified by the respective institution.</p>',
                position: 'left'
            }, {
                element: '#step4',
                intro: "<b>Work Experience</b> <br> <p>Just like your academic qualifications, your work experience can be managed from here. This is the latest experience added by you.</p>",
                position: 'bottom'
            },
            // {
            //     element: '#step5',
            //     intro: '<b>Verify your work experience</b> <br> <p>You can click here to add a suitable supporting document to get your Experience and Salary verified by the respective company.</p>'
            // },
            {
                element: '#step6',
                intro: '<b>Skills</b> <br> <p>You can add or edit your skills from here.</p>'
            }, {
                element: '#step7',
                intro: '<b>Chrome Extension</b> <br> <p>You can download PeopleChain Shield Chrome Extension by clicking “Add to Chrome” post this tour. Shield will generate your PeopleChain Secure Key and will unlock all features a) Document Upload, b) Requests and c) Wallet</p>'
            }
            ].filter(function (obj) {
                return $(obj.element).length;
            })
        });
        intro.start()
        .oncomplete(function() {
            if (!$scope.user.isProfileIntroduced) {
                $('#chromeModal').modal('show');
            }
        })
        .onexit(function() {
            console.log("Tour exited");
            if (!$scope.user.isProfileIntroduced) {
                dataFactory.profileIntroduced(function(response) {
                    console.log(response);
                })
            }
        })
        .onbeforechange(function(element) {
            console.log("Changing")
            if (!element) {
                introJs().nextStep();
            }
        })
    }

    $scope.searchUser = function () {
        if ($scope.searchedUser.length < 1) return;
        dataFactory.searchUser($scope.searchedUser, function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                let users = [];
                response.data.users.forEach((user) => {
                    users.push(user.profile.fname + " " + user.profile.lname);
                })
                autocomplete(document.getElementById('search'), users)
            }
        })
    }

    $scope.connectExtension = function () {
        document.getElementById("install").style.display = "none";
        document.getElementById("detecting").style.display = "block";
        window.open('https://chrome.google.com/webstore/detail/peoplechain-crypto-extens/jaocneammilhbdphalkgilhkenpnnbnd', '_blank', 'location=yes,height=200,width=720,scrollbars=yes,status=yes, left: 100, top: 200');

        $scope.extensionTimer = setInterval(function () {
            let request = {
                method: "connectExt"
            }
            connectToExtension(request, (result) => {
                if (result) {
                    console.log(result);
                    clearInterval($scope.extensionTimer);
                    document.getElementById("install").style.display = "none";
                    document.getElementById("detecting").style.display = "none";
                    document.getElementById("installed").style.display = "block";
                    setTimeout(function () {
                        $scope.createCryptoWallet();
                    }, 2000)
                }
            })
        }, 1000);
    }

    $scope.connectExistingExtension = function () {
        setTimeout(function () {
            $scope.createCryptoWallet();
        }, 2000)
    }

    $scope.createCryptoWallet = function () {
        $("#chromeModal").modal('hide');
        if ($scope.user.address) {
            console.log("Re-create account");
            location.reload();
        } else {
            $("#passwordModal").modal('show');
        }
    }

    $scope.newCryptoAccount = function () {
        let password = document.getElementById('password').value;


        document.getElementById('password').style.border = "unset";

        if (password.length < 8) {
            document.getElementById('password').style.border = "1px solid red";
            document.getElementById('passwordError').innerHTML = "Password should be atleast 8 characters."
            return;
        }

        let request = {
            password: password
        }
        dataFactory.verifyPassword(request, function (response) {
            if (response.status == 200 && response.data && response.data.status == "Success") {
                // move along to create crypto account

                console.log("Creating crypyo account");

                let user = {
                    username: $scope.user.username,
                    password: password
                }
                let request = {
                    method: "createAccount",
                    user: user
                }
                connectToExtension(request, (result) => {

                    console.log(result)

                    if (result) {
                        console.log(result);
                        $("#passwordModal").modal('hide');
                        let request = {
                            "publicKey": result.publicKey
                        }
                        console.log(request)
                        dataFactory.addAddress(request, function (response) {
                            if (response.status == 200 && response.data && response.data.status == "Success") {
                                setTimeout(function () {
                                    window.location.href = '/user/profile';
                                }, 3000)
                            } else {
                                console.log(response);
                            }
                        })
                    }
                })
            } else {
                document.getElementById('password').style.border = "1px solid red";
                document.getElementById('passwordError').innerHTML = response.data.message;
            }
        })
    }

    $scope.fromSecretForm = function () {
        document.getElementById("cryptoFile").value = '';
        document.getElementById('checkCryptoFile').disabled = false;
        document.getElementById("checkCryptoFile").style.display = "none";
        document.getElementById("selectKit").style.border = "unset";
        document.getElementById("selectKit").style.background = "";
        document.getElementById("selectKit").innerHTML = "Upload Kit <span><i class='fas fa-file-upload'></i></span>"
        document.getElementById("fromSecret").style.display = "block";
    }

    $scope.newCryptoAccountFromSecret = function () {
        let secretKey = document.getElementById("secretKey_create").value;
        let password = document.getElementById("pc_password").value;

        console.log(secretKey, password);
        let request = {
            method: "addAccountFromSecret",
            encryptedSecretKey: secretKey,
            password: password,
            address: $scope.user.address,
            username: $scope.user.username
        }
        connectToExtension(request, (result) => {
            if (result.isAdded) {
                location.reload();
            } else {
                document.getElementById("passwordError2").innerHTML = result.info;
            }
        })
    }

    $scope.loadCryptoFile = function () {
        console.log("loading crypto");
        document.getElementById("checkCryptoFile").disabled = true;
        document.getElementById("fromSecret").style.display = "none";
        document.getElementById("checkCryptoFile").style.border = "unset";
        document.getElementById("checkCryptoFile").style.background = "#D3D3D3";
        document.getElementById("cryptoSpinner").style.display = "block";
        var crypto_file = document.getElementById("cryptoFile").files[0];

        console.log(crypto_file);

        let reader = new FileReader();
        reader.onload = function () {
            var text = this.result;
            console.log(text);
            try {
                let account = JSON.parse(text);
                if (account.publicKey == $scope.user.address && account.username == $scope.user.username) {
                    console.log("Correct account");
                    // check if account to be remembered
                    let remember = document.getElementById("remembered").checked;
                    if (remember) {
                        // send account details to plugin to add account
                        let request = {
                            method: "addAccount",
                            account: text
                        }
                        connectToExtension(request, (result) => {
                            sessionStorage.setItem("currentUser", text);
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        })
                    } else {
                        sessionStorage.setItem("currentUser", text);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                } else {
                    console.log("Incorrect account");
                }
            } catch (err) {
                console.log("Invalid file")
            }
        }
        reader.readAsText(crypto_file);

    }

    $scope.cancelExtension = function () {
        clearInterval($scope.extensionTimer);
        document.getElementById("cancelExtension").style.display = "none";
        document.getElementById("loader").style.display = "none";
        document.getElementById("atcBtn").style.display = "block";
    }

    $scope.updateProfile = function(){
        let file = $scope.profilePic;
        let mobile = $scope.user.profile.mobile;
        let location = $scope.user_location;
        
        console.log(file, mobile, location);
        var fd = new FormData();    
        if (file) {
            console.log("Yes file!")
            fd.append('file', file);    
        }

        fd.append('mobile', mobile);
        fd.append('location', location);

        dataFactory.updateProfile(fd, function(response) {
            if (response.status == 200 && response.data && response.data.status == "Success") {
                setTimeout(function() {
                    window.location.href = '/user/profile';
                }, 1000)
            } else {
                console.log(response);
                // TODO: Server response popups
            }
        })
        return false;
    }

    $scope.addAcademic = function (record) {
        $scope.academic = record;
        if ($scope.user.address) {
            $('#add_acacemic_marks_info').modal('show');
        } else {
            $('#chromeModal').modal('show');
        }
    }


    $scope.addProfessional = function (record) {
        $scope.professional = record;
        if ($scope.user.address) {
            $('#addworkexpletters').modal('show');
        } else {
            $('#chromeModal').modal('show');
        }
    }

    $scope.academicUpload = function (record, category) {

        var marks = $scope.private.toString();
        var file = $scope.file;

        $scope.uploadDocument(record, category, file, marks);

    }

    $scope.uploadDocument = function (record, category, file, private) {
        var reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = function () {
            var filedata = this.result
            $scope.encryptResponse = {}
            $scope.shareResponse = {}
            let request = {
                file: filedata,
                private: private,
                signee: record.organization.admin.address,
                recordId: record._id,
                category: category
            }

            $('#add_acacemic_marks_info').modal('hide');
            $('#addworkexpletters').modal('hide');
            window.postMessage({ type: 'encryptRecord', text: request }, domain);

        }
    }

    $scope.verifyMobile = function () {

        dataFactory.sendMobileOtp(function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                sessionStorage.setItem('otpSend', true);
                $scope.otpSend = true;
                document.getElementById('resend').style.display = "none";
                $scope.timeleft = localStorage.getItem("timer") ? localStorage.getItem("timer") : 90;

                if ($scope.timeleft <= 0) {
                    document.getElementById("timer").innerHTML = "OTP Expired";
                    document.getElementById("resend").style.display = "block";
                    return
                } else {
                    $scope.timer = setInterval(function () {
                        document.getElementById("timer").innerHTML = 0 + --$scope.timeleft + " seconds left";
                        localStorage.setItem("timer", $scope.timeleft);
                        if ($scope.timeleft <= 0) {
                            localStorage.setItem("timer", 0);
                            clearInterval($scope.timer);
                            document.getElementById("timer").innerHTML = "OTP expired";
                            document.getElementById("resend").style.display = "block";
                        }
                    }, 1000)
                }
            }
        })
    }

    $scope.resendMobileOtp = function () {
        localStorage.setItem("timer", 90);
        $scope.verifyMobile();
    }

    $scope.verifyMobileOtp = function () {

        let otp = document.getElementById("otp").value;

        if (otp.length != 6 || !$scope.timeleft > 0) {
            console.log("Invalid Otp or timer expired");
            return
        }

        dataFactory.verifyMobileOtp(otp, function (response) {
            console.log(response);
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                $scope.otpSend = false;
                sessionStorage.removeItem("otpSend");
                $window.location.href = '/user/profile';
            }
        })
    }

    // $("#addeducationalInfo").on('shown.bs.collapse') {
    //     console.log("adding education")
    // }

    // $scope.addEducation = function () {
    //     if (isValid) {
    //         var record;
    //         var fromDate = document.getElementById("startdate").value.trim();
    //         var toDate = document.getElementById("enddate").value.trim();
    //         var startdate, enddate, from, to;

    //         if (fromDate == "" && toDate == "") {
    //             var span = document.getElementById("error_from");
    //             span.innerHTML = "Please select dates";
    //             //document.getElementById("loader").style.display = "none";
    //             span.style.display = "block";
    //         }
    //         else if (fromDate == "" && toDate != null) {
    //             var span = document.getElementById("error_from");
    //             span.innerHTML = "Please select from date";
    //             //document.getElementById("loader").style.display = "none";
    //             span.style.display = "block";
    //         }
    //         else if (toDate == "" && fromDate != "") {
    //             if (fromDate.length != 7) {
    //                 var span = document.getElementById("error_from");
    //                 span.innerHTML = "Invalid from date";
    //                 //document.getElementById("loader").style.display = "none";
    //                 span.style.display = "block";
    //             } else {
    //                 enddate = "Present";
    //                 var splittedFrom = fromDate.split("/");
    //                 from = splittedFrom[0] + "/01/" + splittedFrom[1];
    //                 startdate = new Date(from);
    //                 let orgname = document.getElementById("cname").value;
    //                 record = {
    //                     orgName: orgname,
    //                     from: startdate,
    //                     to: enddate,
    //                     role: ($scope.role) ? $scope.role : $scope.ctype + " of " + $scope.cspecial,
    //                     department: $scope.department,
    //                     location: ($scope.location) ? $scope.location : "Null",
    //                     type: "1"
    //                 }
    //                 add(record);
    //             }
    //         }
    //         else {
    //             if (fromDate.length != 7) {
    //                 var span = document.getElementById("error_from");
    //                 span.innerHTML = "Invalid from date";
    //                 //document.getElementById("loader").style.display = "none";
    //                 span.style.display = "block";
    //             }
    //             else if (toDate.length != 7) {
    //                 var span = document.getElementById("error_from");
    //                 span.innerHTML = "Invalid to date";
    //                 //document.getElementById("loader").style.display = "none";
    //                 span.style.display = "block";
    //             } else if (toDate.length != 7 && fromDate.length != 7) {
    //                 var span = document.getElementById("error_from");
    //                 span.innerHTML = "Invalid dates";
    //                 //document.getElementById("loader").style.display = "none";
    //                 span.style.display = "block";
    //             } else {
    //                 var splittedFrom = fromDate.split("/");
    //                 from = splittedFrom[0] + "/01/" + splittedFrom[1];

    //                 var splittedTo = toDate.split("/");
    //                 to = splittedTo[0] + "/01/" + splittedTo[1];

    //                 startdate = new Date(from);
    //                 enddate = new Date(to);

    //                 let orgname = document.getElementById("cname").value;

    //                 record = {
    //                     orgName: orgname,
    //                     from: startdate,
    //                     to: enddate,
    //                     role: ($scope.role) ? $scope.role : $scope.ctype + " of " + $scope.cspecial,
    //                     department: $scope.department,
    //                     location: ($scope.location) ? $scope.location : "Null",
    //                     type: "1"
    //                 }
    //                 add(record);
    //             }
    //         }

    //         function add(record) {
    //             console.log(record)
    //             var currentDate = new Date();

    //             if (record.to != "Present") {
    //                 //compare 2 dates
    //                 if (record.from > record.to) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "From date can not be greater than to date";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 }
    //                 else if (record.from < new Date("01/01/1970")) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "Invalid from date, select date after 1970";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 }
    //                 else if (record.to > new Date("12/01/2023")) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "To date can not cross 2023";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 }
    //                 else if (record.from > currentDate) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "From can not be future date, kindly select valid one";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 } else {
    //                     console.log("success");
    //                     document.getElementById("error_from").style.display = "none";
    //                     dataFactory.addRecord(record, function (response) {
    //                         if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
    //                             location.reload();
    //                         } else {
    //                             console.log("Unable to add record.")
    //                         }
    //                     })
    //                 }
    //             } else {
    //                 if (record.from > currentDate) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "From can not be future date, kindly select valid one";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 }
    //                 else if (record.from < new Date("01/01/1970")) {
    //                     var span = document.getElementById("error_from");
    //                     span.innerHTML = "Invalid from date, select date after 1970";
    //                     //document.getElementById("loader").style.display = "none";
    //                     span.style.display = "block";
    //                 }
    //                 else {
    //                     console.log("success");
    //                     document.getElementById("error_from").style.display = "none";
                        // dataFactory.addRecord(record, function (response) {
                        //     if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                        //         location.reload();
                        //     } else {
                        //         console.log("Unable to add record.")
                        //     }
                        // })
    //                 }
    //             }
    //         }
    //     }
    // }

    $scope.addEducation = function() {
        console.log("adding education");

        var record = {};
        var span = document.getElementById('error_from');
        if ($scope.qualification == 3) {
            // add school record
            var passDate = $scope.passyear;
            let orgname = document.getElementById("cname").value;
        
            if (passDate == "") {
                span.innerHTML = "Please select a valid graduation date";
                span.style.display = "block";
                return
            }
        
            passDate = new Date(passDate);
        
            record = {
                orgName: orgname,
                from: null,
                to: passDate,
                role: $scope.sboard,
                department: $scope.grade,
                location: "",
                type: $scope.qualification,
                record_type: "fulltime"
            }
        } else {
            // add other education record

            var joinYear = $scope.jdate;
            var passYear = $scope.upyear;

            if (joinYear == "") {
                span.innerHTML = "Please select a start date";
                return
            }

            if (passYear == "" || passYear == null) {
                passYear = "Present";
            } else {
                passYear = new Date(passYear)
            }

            joinYear = new Date(joinYear);
            let currentDate = new Date();

            if (currentDate < joinYear) {
                span.innerHTML = "Start date cannot be in the future";
                span.style.display = "block";
                return
            }

            if (passYear != "Present" && passYear < joinYear) {
                span.innerHTML = "Start date cannot be greater than end date";
                span.style.display = "block";
                return
            }

            let orgname = document.getElementById("uname").value;

            record = {
                orgName: orgname,
                from: joinYear,
                to: passYear,
                role: $scope.ucourse,
                department: $scope.specialization,
                location: "",
                type: $scope.qualification,
                record_type: $scope.ctype 
            }
        }

        dataFactory.addRecord(record, function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                location.reload();
            } else {
                console.log("Unable to add record.")
            }
        })
    }

    $scope.updateEducationModal = function (record) {
        console.log(record);
        $scope.currentEducationRecord = record;
        $scope.institution = record.organization.orgname;
        $("#editEducationalInfo").modal('show');
    }

    $scope.updateRecord = function (record) {
        console.log("Clicked")
        console.log(record);
        var updated_record;
        var fromDate = document.getElementById("startdate2").value.trim();
        var toDate = document.getElementById("enddate2").value.trim();
        var startdate, enddate, from, to;

        if (fromDate == "" && toDate == "") {
            var span = document.getElementById("error_from_updateEdu");
            span.innerHTML = "Please select dates";
            //document.getElementById("loader").style.display = "none";
            span.style.display = "block";
        }
        else if (fromDate == "" && toDate != null) {
            var span = document.getElementById("error_from_updateEdu");
            span.innerHTML = "Please select from date";
            //document.getElementById("loader").style.display = "none";
            span.style.display = "block";
        }
        else if (toDate == "" && fromDate != "") {
            if (fromDate.length != 7) {
                var span = document.getElementById("error_from_updateEdu");
                span.innerHTML = "Invalid from date";
                //document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            } else {
                enddate = "Present";
                var splittedFrom = fromDate.split("/");
                from = splittedFrom[0] + "/01/" + splittedFrom[1];
                startdate = new Date(from);
                // let orgname = document.getElementById("cname").value;
                updated_record = {
                    id: record._id,
                    orgName: $scope.cname2,
                    from: startdate,
                    to: enddate,
                    role: $scope.ctype2 + " of " + $scope.cspecial2,
                    department: $scope.department2
                }
                updateEduRecord(updated_record);
            }
        }
        else {
            if (fromDate.length != 7) {
                var span = document.getElementById("error_from_updateEdu");
                span.innerHTML = "Invalid from date";
                //document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            }
            else if (toDate.length != 7) {
                var span = document.getElementById("error_from_updateEdu");
                span.innerHTML = "Invalid to date";
                //document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            } else if (toDate.length != 7 && fromDate.length != 7) {
                var span = document.getElementById("error_from_updateEdu");
                span.innerHTML = "Invalid dates";
                //document.getElementById("loader").style.display = "none";
                span.style.display = "block";
            } else {
                var splittedFrom = fromDate.split("/");
                from = splittedFrom[0] + "/01/" + splittedFrom[1];

                var splittedTo = toDate.split("/");
                to = splittedTo[0] + "/01/" + splittedTo[1];

                startdate = new Date(from);
                enddate = new Date(to);

                let orgname = document.getElementById("cname").value;

                updated_record = {
                    id: record._id,
                    orgName: $scope.cname2,
                    from: startdate,
                    to: enddate,
                    role: $scope.ctype2 + " of " + $scope.cspecial2,
                    department: $scope.department2
                }
                updateEduRecord(updated_record);
            }
        }

        function updateEduRecord(updated_record) {
            var currentDate = new Date();

            if (updated_record.to != "Present") {
                //compare 2 dates
                if (record.from > record.to) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "From date can not be greater than to date";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else if (record.from < new Date("01/01/1970")) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "Invalid from date, select date after 1970";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else if (record.to > new Date("12/01/2023")) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "To date can not cross 2023";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else if (updated_record.from > currentDate) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "From can not be future date, kindly select valid one";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                } else {
                    document.getElementById("error_from_updateEdu").style.display = "none";
                    dataFactory.updateRecord(updated_record, function (response) {
                        if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                            location.reload();
                        } else {
                            console.log("Unable to update record");
                        }
                    })
                }
            } else {
                if (updated_record.from > currentDate) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "From can not be future date, kindly select valid one";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else if (record.from < new Date("01/01/1970")) {
                    var span = document.getElementById("error_from_updateEdu");
                    span.innerHTML = "Invalid from date, select date after 1970";
                    //document.getElementById("loader").style.display = "none";
                    span.style.display = "block";
                }
                else {
                    console.log("success");
                    document.getElementById("error_from_updateEdu").style.display = "none";
                    dataFactory.updateRecord(updated_record, function (response) {
                        if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                            location.reload();
                        } else {
                            console.log("Unable to update record");
                        }
                    })
                }
            }
        }
    }

    $scope.viewAcademic = function(record) {
        console.log("View document modal");
        $scope.academic = record;
        $scope.marksheets = [];
        $scope.provisionals = [];
        $scope.degrees = [];
        record.documents.forEach((document) => {
            if (document.category == "degree") {
                $scope.degrees.push(document);
            }

            if (document.category == "provisional") {
                $scope.provisionals.push(document);
            }

            if (document.category == "marksheet") {
                $scope.marksheets.push(document);
            }
        })
        $("#viewacademicdocs").modal('show');
    }

    $scope.viewProfessional = function(record) {
        console.log(record)
        $scope.professional = record;
        $scope.appointments = [];
        $scope.rels = [];
        $scope.payslips = [];
        record.documents.forEach((document) => {
            if (document.category == "payslip") {
                $scope.payslips.push(document);
            }

            if (document.category == "appointment") {
                $scope.appointments.push(document);
            }

            if (document.category == "relieving") {
                $scope.rels.push(document);
            }
        })
        console.log($scope.payslips)
        $("#viewworkexpdocs").modal('show');
    }

    $scope.addWork = function(isValid) {
        if (isValid) {
            var record;
    
            var fromDate = $scope.wstartdate;
            var toDate = $scope.wenddate;
            var span = document.getElementById("error_from_work");
            
            let orgname = document.getElementById('wcompany').value;
    
            if (fromDate == "") {
                span.innerHTML = "Please select start date";
                span.style.display = "block";
                return
            } else {
                if (toDate == "" || toDate == null) {
                    toDate = "Present";
                } else {
                    toDate = new Date(toDate);
                }
    
                fromDate = new Date(fromDate);
    
                let currentDate = new Date();
    
                if (currentDate < fromDate) {
                    span.innerHTML = "Start date cannot be in the future";
                    span.style.display = "block";
                    return
                }
    
                if (toDate != "Present" && toDate < fromDate) {
                    span.innerHTML = "Start date cannot be greater than end date"
                    span.style.display = "block";
                    return
                }
    
                record = {
                    orgName: orgname,
                    from: new Date(fromDate),
                    to: toDate,
                    role: $scope.wrole,
                    department: ($scope.wdepartment) ? $scope.wdepartment : "Null",
                    location: $scope.wlocation,
                    type: "2",
                    record_type: $scope.etype
                }
                dataFactory.addRecord(record, function (response) {
                    if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                        location.reload();
                    } else {
                        console.log("Unable to add record");
                    }
                })
            }
        }
    }

    // $scope.addSkill = function() {
    //     console.log($scope.skillSearch);
    //     let skill = document.getElementById('skill').value;
    //     console.log("Skill adding" + skill);
    //     console.log($scope.skills)
    //     if ($scope.skills.includes(skill) && $scope.mySkills.length < 6 && !$scope.mySkills.includes(skill)) {
    //         $scope.newSkills.push(skill);
    //         $scope.mySkills.push(skill);
    //         document.getElementById('skill').value = '';
    //     }
    // }

    $("#skill").on('change', function (e) {
        var skill = document.getElementById("skill").value;
        console.log("walah" + skill);
        if ($scope.skills.includes(skill) && $scope.mySkills.length < 6 && !$scope.mySkills.includes(skill)) {
            $scope.$apply(function () {
                $scope.newSkills.push(skill);
                $scope.mySkills.push(skill);
            })
            document.getElementById('skill').value = '';
        } else {
            console.log("some issue");
        }

        // $scope.newSkills.push(skill);
        // $scope.mySkills.push(skill);
        // console.log($scope.newSkills);
        // console.log($scope.mySkills);
        // document.getElementById("skill").value = '';
    })

    $scope.deleteMySkill = function (skill) {
        var index = $scope.mySkills.indexOf(skill);
        if (index > -1) {
            $scope.deletedSkills.push($scope.mySkills[index]);
            console.log($scope.deletedSkills);
            $scope.mySkills.splice(index, 1);
        }
    }

    $("#addskills").on('hidden.bs.modal', function (e) {
        $scope.$apply(function () {
            $scope.mySkills = $scope.mySkills.filter((skill) => {
                return !$scope.newSkills.includes(skill);
            })
            $scope.mySkills = $scope.mySkills.concat($scope.deletedSkills);
            $scope.deletedSkills = [];
            $scope.newSkills = [];
        })
    });

    $scope.updateSkills = function () {
        let request = {
            skills: $scope.mySkills
        }

        dataFactory.submitSkills(request, function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                location.reload();
            }
        })
    }

    $scope.logout = function () {
        console.log("Logging out")
        var i = sessionStorage.length;
        while (i--) {
            var key = sessionStorage.key(i);
            sessionStorage.removeItem(key);
        }
        dataFactory.logout(function (response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                window.location.href = '/signin';
            }
        });
    }

    $window.addEventListener('message', function (event) {

        if (event.source !== $window) return;

        if (event.data.type == "onEncryption") {
            dataFactory.addDocument(event.data.text, function (response) {
                if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                    // showacademicupload();
                    setTimeout(function () {
                        location.reload();
                    }, 2000)
                } else {
                    console.log(response);
                    // Server response popup
                }
            })
        }

        if (event.data.type == "accountCreated") {
            console.log("Account created");
        }

        if (event.data.type == "accountCreateFailed") {
            console.log("Failed to create account");
        }

        if (event.data.type == "logout") {
            $scope.logout();
        }
    })

    $("#chromeModal").on("hidden.bs.modal", function (e) {
        clearInterval($scope.extensionTimer);
        document.getElementById("installed").style.display = "none";
        document.getElementById("detecting").style.display = "none";
        document.getElementById("install").style.display = "block";
    })

    $("#chromeModal").on("shown.bs.modal", function (e) {

        console.log("Opening Modal");

        $('.bxslider').bxSlider({
            auto: true
        });

        let request = {
            method: "connectExt"
        }

        connectToExtension(request, (result) => {
            if (result) {
                $scope.extensionInstalled = true;
            } else {
                $scope.extensionInstalled = false;
            }
            return;
        })
    })

    $("#reviewModal").on("shown.bs.modal", function (e) {
        var slider = $('.bxslider1').bxSlider({
            infiniteLoop: false,
            hideControlOnEnd: true,
            pager: false,
            controls: false,
            // touchEnabled: false
        });

        var next_btn = document.getElementById("next_btn");
        next_btn.click = function () {
            // var current = slider.getCurrentSlide();
            // console.log(current);
            // slider.goToSlide(current + 1);
            // return false;
            console.log("next butn")
            slider.goToNextSlide();
        }

        var next_btn2 = document.getElementById("next_btn2");
        next_btn2.click = function () {
            var current = slider.getCurrentSlide();
            slider.goToSlide(current + 1);
            return false;
        }

        var next_btn3 = document.getElementById("next_btn3");
        next_btn3.click = function () {
            document.getElementById("review_block").style.display = "none";
            document.getElementById("summery_block").style.display = "block";
            return false;
        }

        var download_btn = document.getElementById("download_btn");
        download_btn.click = function () {
            var account = JSON.parse(sessionStorage.getItem("currentUser"))
            var text = JSON.stringify({
                username: account.username,
                publicKey: account.publicKey,
                encryptedSecretKey: account.encryptedSecretKey
            })
            var element = document.createElement('a')
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text))
            element.setAttribute('download', "keyPair.txt")

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
            var current = slider.getCurrentSlide();
            slider.goToSlide(current + 1);
            return false;
        }
    })

    $scope.finish = function () {
        console.log("Finished");
        document.getElementById("summery_block").style.display = "none";
        document.getElementById("thankyou_block").style.display = "block";
        dataFactory.userInformed((response) => {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                setTimeout(function () {
                    location.reload();
                }, 4000);
            }
        })
    }
})

app.controller('requestCtrl', function ($scope, $window, dataFactory) {

    $scope.getRequests = function () {
        dataFactory.getRequests(function (response) {
            console.log(response);
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {

                $scope.isAdmin = response.data.isAdmin;
                $scope.thisUser = response.data.user;
                $scope.processing = [];
                $scope.verified = [];
                $scope.declined = [];
                $scope.all_sent = [];
                $scope.totalDocs = 0;
                console.log(response.data.records)
                for (var i = 0; i < response.data.records.length; i++) {
                    response.data.records[i].documents.forEach((document) => {
                        $scope.totalDocs = $scope.totalDocs + 1
                        document.organization = response.data.records[i].organization;
                        document.date = new Date(document.submittedOn).toLocaleDateString();
                        document.time = new Date(document.submittedOn).toLocaleTimeString();
                        document.category = document.category.toUpperCase();
                        $scope.all_sent.push(document);
                        if (document.status == 'PENDING') {
                            $scope.processing.push(document);
                        } else if (document.status == 'SIGNED') {
                            $scope.verified.push(document);
                        } else {
                            $scope.declined.push(document);
                        }
                    })
                }

                $scope.ar_sent_all = [];
                $scope.ar_sent_processing = [];
                $scope.ar_sent_verified = [];
                $scope.ar_sent_declined = [];
                $scope.ar_recd_all = [];
                $scope.ar_recd_processing = [];
                $scope.ar_recd_verified = [];
                $scope.ar_recd_declined = [];
                $scope.reqSent = 0;
                $scope.reqRecd = 0;

                $scope.currentUser = sessionStorage.getItem('currentUser');

                if ($scope.currentUser) {
                    $scope.user = JSON.parse($scope.currentUser).publicKey;

                    for (var j = 0; j < response.data.requests.length; j++) {
                        let request = response.data.requests[j]
                        request.date = new Date(request.createdOn).toLocaleDateString();
                        request.time = new Date(request.createdOn).toLocaleTimeString();
                        request.category = request.document.category.toUpperCase();
                        if (request.requestingUser.address == $scope.user) {
                            // Sent requests
                            $scope.reqSent += $scope.reqSent + 1;
                            $scope.ar_sent_all.push(request);
                            if (request.status == 'PENDING') {
                                $scope.ar_sent_processing.push(request);
                            } else if (request.status == 'GRANTED') {
                                $scope.ar_sent_verified.push(request);
                            } else {
                                $scope.ar_sent_declined.push(request);
                            }
                        } else {
                            // Recvd requests
                            $scope.reqRecd = $scope.reqRecd + 1;
                            $scope.ar_recd_all.push(request);
                            if (request.status == 'PENDING') {
                                $scope.ar_recd_processing.push(request);
                            } else if (request.status == 'GRANTED') {
                                $scope.ar_recd_verified.push(request);
                            } else {
                                $scope.ar_recd_declined.push(request);
                            }
                        }
                    }

                    if ($scope.isAdmin) {
                        $scope.sr_all = [];
                        $scope.sr_processing = [];
                        $scope.sr_verified = [];
                        $scope.sr_declined = [];
                        $scope.totalSR = 0;
                        $scope.signRequests = response.data.signRequests

                        $scope.signRequests.forEach((request) => {
                            request.documents.forEach((document) => {
                                document.date = new Date(document.submittedOn).toLocaleDateString();
                                document.time = new Date(document.submittedOn).toLocaleTimeString();
                                document.user = request.user;
                                document.category = document.category.toUpperCase();
                                $scope.totalSR = $scope.totalSR + 1;
                                $scope.sr_all.push(document);
                                if (document.status == 'PENDING') {
                                    $scope.sr_processing.push(document);
                                }
                                if (document.status == 'SIGNED') {
                                    $scope.sr_verified.push(document);
                                }

                                if (document.status == 'DECLINED') {
                                    $scope.sr_declined.push(document);
                                }
                            })
                        })
                    }

                    $("#pageLoader").fadeOut();

                    // Check conditions and launch the tour
                    if (!$scope.thisUser.isRequestIntroduced) {
                        introJs().start()
                            .oncomplete(function () {
                                console.log("Completed tour")
                            })
                            .onexit(function () {
                                console.log("Tour exited");
                                dataFactory.requestIntroduced(function (response) {
                                    console.log(response);
                                })
                            })
                    }

                } else {
                    $("#pcUnlockShieldModal").modal({
                        backdrop: "static",
                        keyboard: false
                    });
                    $("#pageLoader").fadeOut();
                }
            }
        })
    }

    $scope.logout = function() {
        console.log("Logging out")
        var i = sessionStorage.length;
        while(i--) {
            var key = sessionStorage.key(i);
            sessionStorage.removeItem(key); 
        }
        dataFactory.logout(function(response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                window.location.href = '/signin';
            }
        });
    }

    $scope.signDocument = function (id, hash, action) {

        let request = {};
        request.id = id;
        request.action = action;
        request.hash = hash;

        window.postMessage({ type: 'signRecord', text: request }, domain);
    }

    $scope.accessGranted = function (id, address, key) {
        let request = {};
        request.id = id;
        request.address = address;
        request.key = key;
        request.action = "GRANTED";

        window.postMessage({ type: 'shareKey', text: request }, domain);
    }

    $scope.accessDeclined = function (id) {
        let request = {};
        request.id = id;
        request.action = "DECLINED";
        request.sharedKey = '';

        dataFactory.handleAccess(request, function (response) {
            console.log(response);
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                setTimeout(function () {
                    location.reload();
                }, 2000)
            }
        })
    }

    $scope.decryptOther = function (request) {

        dataFactory.getFromIpfs(request.document.multihash, function (response) {
            if (response.data) {
                let document = {};
                document.private = request.document.private;
                document.file = response.data;
                document.sharedKey = request.sharedKey;
                document.address = request.requestedUser.address;

                window.postMessage({ type: 'decrypt', text: document }, domain);
            }
        })
    }

    $scope.decryptRecvd = function (request) {

        dataFactory.getFromIpfs(request.multihash, function (response) {
            let document = {};
            document.private = request.private;
            document.file = response.data;
            document.sharedKey = request.adminKey;
            document.address = request.user.address;

            window.postMessage({ type: 'decrypt', text: document }, domain);
        })
    }

    $scope.decryptSent = function(request) {
        
        console.log(request)
        dataFactory.getFromIpfs(request.multihash, function(response) {
            let document = {};
            document.private = request.private;
            document.file = response.data;
            document.sharedKey = request.userKey;
            document.address = $scope.thisUser.address;

            window.postMessage({type: 'decrypt', text: document}, domain);
        })
    }

    $scope.redirectAndInstall = function() {
        window.location = '/user/profile#chromeModal'
    }

    $window.addEventListener('message', function (event) {

        if (event.source !== $window) return;

        if (event.data.type && event.data.type == "onSignature") {
            dataFactory.signDocument(event.data.text, function (response) {
                console.log(response);
                if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                    setTimeout(function () {
                        location.reload();
                    }, 2000)
                }
            })
        }

        if (event.data.type && event.data.type == 'keyShared') {
            console.log(event.data.text)
            dataFactory.handleAccess(event.data.text, function (response) {
                console.log(response);
                if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                    setTimeout(function () {
                        location.reload();
                    }, 2000)
                }
            })
        }

        if (event.data.type == 'decrypted') {
            console.log(event.data.text);
            document.getElementById("docPvtData").innerHTML = event.data.text.privateData;
            document.getElementById("docPvtImg").src = event.data.text.file;
            $("#viewDocmentModal").modal('show');
        }

        if (event.data.type == "logout") {
            $scope.logout();
        }
    })

})

app.controller('walletCtrl', function($scope, $window, dataFactory) {

    $scope.getUser = function () {
        dataFactory.getUser(function (response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {

                $scope.thisUser = response.data.user;
                $scope.fullname = response.data.user.profile.fname + " " + response.data.user.profile.lname;
                $scope.balance = response.data.user.balance;
                $scope.userId = response.data.user._id;
                $scope.dateJoined = new Date(response.data.user.createdAt).toLocaleDateString();
                $scope.timeJoined = new Date(response.data.user.createdAt).toLocaleTimeString();
                $scope.transactions = []
                $scope.totalTransactions = 0;

                // Transactions for record added by the user
                for (let i = 0; i < response.data.user.records.length; i++) {
                    response.data.user.records[i].documents.forEach((document) => {
                        let transaction = {};
                        transaction.orgname = response.data.user.records[i].organization.orgname;
                        transaction.orgid = response.data.user.records[i].organization.orgid;
                        transaction.id = document._id;
                        transaction.category = document.category.toUpperCase();
                        transaction.submittedOn = document.submittedOn;
                        transaction.date = new Date(document.submittedOn).toLocaleDateString();
                        transaction.time = new Date(document.submittedOn).toLocaleTimeString();
                        transaction.type = "Debited";
                        transaction.purpose = "Verification";
                        transaction.value = 500;
                        $scope.transactions.push(transaction);
                    })
                }
                $scope.currentUser = sessionStorage.getItem('currentUser');

                if ($scope.currentUser) {
                    $scope.user = JSON.parse($scope.currentUser).publicKey;

                    // Transactions for document access
                    response.data.requests.forEach((request) => {
                        if (request.status == 'GRANTED') {
                            console.log(request.signedOn + " Hello")
                            let transaction = {};
                            transaction.id = request._id;
                            transaction.category = request.document.category.toUpperCase();
                            transaction.submittedOn = request.signedOn;
                            transaction.date = new Date(request.signedOn).toLocaleDateString();
                            transaction.time = new Date(request.signedOn).toLocaleTimeString();
                            transaction.value = 300;
                            transaction.purpose = "Access";
                            if ($scope.user == request.requestingUser.address) {
                                transaction.type = "Debited";
                                transaction.orgname = request.requestedUser.profile.fname + " " + request.requestedUser.profile.lname;
                                transaction.orgid = request.requestedUser.username;
                            } else if ($scope.user == request.requestedUser.address) {
                                transaction.type = "Credited";
                                transaction.orgname = request.requestingUser.profile.fname + " " + request.requestingUser.profile.lname;
                                transaction.orgid = request.requestingUser.username;
                            }
                            $scope.transactions.push(transaction);
                        }
                    })

                    // Transactions for admin user - recvd for signature
                    if (response.data.user.admin.isAdmin) {
                        $scope.signRequests = response.data.signRequest;
                        $scope.signRequests.forEach((request) => {

                            request.documents.forEach((document) => {
                                console.log(document + " This is a documenent.")
                                let transaction = {};
                                transaction.orgname = request.user.profile.fname + " " + request.user.profile.lname;
                                transaction.orgid = request.user.username;
                                transaction.id = document._id;
                                transaction.category = document.category.toUpperCase();
                                transaction.date = new Date(document.submittedOn).toLocaleDateString();
                                transaction.time = new Date(document.submittedOn).toLocaleTimeString();
                                transaction.submittedOn = document.submittedOn;
                                transaction.type = "Credited";
                                transaction.purpose = "Verification";
                                transaction.value = 500;
                                $scope.transactions.push(transaction);
                            })

                        })
                    }

                    // Transactions from payments
                    response.data.payments.forEach((payment) => {
                        let transaction = {};
                        if (payment.to.address && payment.to.address == $scope.user) {
                            transaction.orgname = payment.from.profile.fname + " " + payment.from.profile.lname;
                            transaction.orgid = payment.from.username;
                            transaction.id = payment._id;
                            transaction.date = new Date(payment.createdOn).toLocaleDateString();
                            transaction.time = new Date(payment.createdOn).toLocaleTimeString();
                            transaction.submittedOn = payment.createdOn;
                            transaction.type = "Credited";
                            transaction.purpose = payment.message;
                            transaction.value = payment.amount;
                            $scope.transactions.push(transaction);
                        }
                    })

                    $("#pageLoader").fadeOut();
                    console.log($scope.transactions)

                    if (!$scope.thisUser.isWalletIntroduced) {

                        $("#pcWalletTockensModal").modal({
                            backdrop: "static",
                            keyboard: false
                        })
                    }
                } else {
                    $("#pcUnlockShieldModal").modal({
                        backdrop: "static",
                        keyboard: false
                    });
                    $("#pageLoader").fadeOut();
                }
            }
        })
    }

    $scope.logout = function() {
        console.log("Logging out")
        var i = sessionStorage.length;
        while(i--) {
            var key = sessionStorage.key(i);
            sessionStorage.removeItem(key); 
        }
        dataFactory.logout(function(response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                window.location.href = '/signin';
            }
        });
    }

    $scope.redirectAndInstall = function() {
        window.location = '/user/profile#chromeModal'
    }

    $scope.startTour = function () {
        $("#pcWalletTockensModal").modal('hide');
        introJs().start()
            .oncomplete(function () {
                console.log("Completed tour")
            })
            .onexit(function () {
                console.log("Tour exited");
                dataFactory.walletIntroduced(function (response) {
                    console.log(response);
                })
            })
    }

    $window.addEventListener('message', function(event) {

        if (event.source !== $window) return;

        if (event.data.type == "logout") {
            $scope.logout();
        }
    })

})

app.controller('publicProfileController', function($scope, $location, $window, dataFactory) {

    $scope.getUser = function () {
        let user = $location.path().split('/').pop()
        dataFactory.getUserPublic(user, function (response) {
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                console.log(response)
                $scope.user = response.data.user;
                $scope.current_user = response.data.current_user;
                $scope.profilePic = ($scope.user.profile.avatar) ? domain + $scope.user.profile.avatar : $scope.user.profile.displayPic;
                $scope.mySkills = response.data.user.skills;

                $scope.education = [];
                $scope.work = [];
                $scope.edu_org = [];
                $scope.work_org = [];
                
                // if ($scope.user.address) {
                //     let qr_text = "https://alpha.peoplechain.org/user/" + $scope.user.username;
                //     new QRCode(document.getElementById("qrcode"), qr_text);
                // }
                if ($scope.user.records.length > 0) {
                    $scope.user.records.forEach((record) => {

                        record.orgVerified = false;

                        for (let i = 0; i < record.documents.length; i++) {
                            if (record.documents[i].status == "SIGNED") {
                                record.orgVerified = true;
                                break;
                            }
                            i++;
                        }
                        
                        if (record.type == "3" || record.type == "4" || record.type == "5") {
                            record.endDate = new Date(record.endDate).toLocaleDateString().split('/')[2];
                            $scope.education.push(record);
                        } else if (record.type == "2") {
                            record.startDate = new Date(record.startDate).toLocaleDateString();
                            record.endDate = new Date(record.endDate).toLocaleDateString();
                            console.log(record.endDate);
                            if (record.endDate == '01/01/1970') {
                                record.endDate = "Present"
                            }
                            $scope.work.push(record);
                        }
                    })
                }

                $("#pageLoader").fadeOut();
            } else {
                console.log(response);
                console.log("Could not find user");
            }
        })
    }

    $scope.viewDocument = function(record) {
        if ($scope.current_user) {
            console.log(record)
            if (record.type == '1') {
                $scope.activaTab('academic');
                // Prepare the orgs for select box

            } else if (record.type == '2') {
                $scope.activaTab('work')
            }
            $("#multipleOrgModal").modal("show");
        } else {
            $scope.displayErrorMessages("Please login to view documents.")
        }
    }

    $scope.activaTab = function (tab) {
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };

    $scope.displayErrorMessages = function(message) {
        document.getElementById("server_response").innerHTML = message;
        $('.serever_mesg_block').fadeIn();
        setTimeout(function() {
            $('.serever_mesg_block').fadeOut();
        }, 5000)
    }

    $scope.displaySuccessMessage = function(message) {
        document.getElementById("server_response").innerHTML = message;
        $('.serever_mesg_block').fadeIn();
        setTimeout(function() {
            $('.serever_mesg_block').fadeOut();
        }, 3000)
    }

    $scope.viewEducation = function(record) {

        console.log("View academic");
        if ($scope.current_user) {
            if (!record.orgVerified) {
                $scope.displayErrorMessages("No documents attached");
                return;
            }
            console.log("View academic");
            console.log(record);
            $scope.academic = record;
            $scope.marksheets = [];
            $scope.provisionals = [];
            $scope.degrees = [];
            record.documents.forEach((document) => {
                if (document.category == "degree") {
                    $scope.degrees.push(document);
                }

                if (document.category == "provisional") {
                    $scope.provisionals.push(document);
                }

                if (document.category == "marksheet") {
                    $scope.marksheets.push(document);
                }
            })
            $("#viewacademicdocs").modal('show');
        } else {
            $scope.displayErrorMessages("Please login to view documents.");
        }
    }

    $scope.viewProfessional = function(record) {
        if ($scope.current_user) {
            if (!record.orgVerified) {
                $scope.displayErrorMessages("No documents attached");
                return;
            }
            console.log(record)
            $scope.professional = record;
            $scope.appointments = [];
            $scope.rels = [];
            $scope.payslips = [];
            record.documents.forEach((document) => {
                if (document.category == "payslip") {
                    $scope.payslips.push(document);
                }

                if (document.category == "appointment") {
                    $scope.appointments.push(document);
                }

                if (document.category == "relieving") {
                    $scope.rels.push(document);
                }
            })
            console.log($scope.payslips)
            $("#viewWorkDocs").modal('show');
        } else {
            $scope.displayErrorMessages("Please login to view documents.");
        }
    }

    $scope.handlePayment = function(record, document) {
        $scope.requestedRecord = record;
        $scope.requestedDocument = document;
        console.log($scope.current_user.balance + " Balance");
        if ($scope.current_user.balance > 250) {
            console.log("Enough balance");
            $("#requestDocument").attr('disabled', false);
        }
        $("#paymentInfoModal").modal('show');
    }

    $scope.requestDocument = function (record, document) {
        console.log($scope.user)
        console.log(record);

        let request = {
            requestedUser: $scope.user._id,
            record: record._id,
            document: document._id
        }

        console.log(request);

        dataFactory.requestDocument(request, function(response) {
            console.log(response);
            if (response.status == 200 && response.data && response.data.status && response.data.status == "Success") {
                $scope.displaySuccessMessage("Successfully requested document from " + $scope.user.profile.fname + " " + $scope.user.profile.lname + ".");
                $("#paymentInfoModal").modal('hide');
            } else {
                $scope.displayErrorMessages(response.data.message);
                $("#paymentInfoModal").modal('hide');
            }
        })
    }

    $scope.logout = function() {
        console.log("Logging out")
        var i = sessionStorage.length;
        while(i--) {
            var key = sessionStorage.key(i);
            sessionStorage.removeItem(key); 
        }
        dataFactory.logout(function(response) {
            console.log(response)
            if (response.status == 200 && response.data && response.data.status && response.data.status == 'Success') {
                window.location.href = '/signin';
            }
        });
    }

    $window.addEventListener('message', function(event) {

        if (event.source !== $window) return;

        if (event.data.type == "logout") {
            $scope.logout();
        }
    })
})

app.factory('dataFactory', function ($http) {

    var factory = {}

    factory.getUser = function (callback) {
        const url = '/user';
        $http.get(url).then(function (output) {
            callback(output);
        });
    };

    factory.getUserPublic = function (user, callback) {
        const url = '/user/profile/' + user;
        $http.get(url).then(function (output) {
            callback(output);
        });
    };

    factory.searchUser = function (name, callback) {
        const url = '/user/search/' + name;
        $http.get(url).then(function (output) {
            callback(output);
        })
    }

    factory.addAddress = function (request, callback) {
        const url = '/user/address'
        $http.post(url, request).then(function (output) {
            callback(output);
        })
    }

    factory.updateProfile = function (request, callback) {
        const url = '/user/profile/update';
        $http.post(url, request, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function(output) {
            callback(output);
        })
    }

    factory.addDocument = function (document, callback) {
        const url = '/user/record/document';
        $http.post(url, document).then(function (output) {
            callback(output);
        })
    }

    factory.sendMobileOtp = function (callback) {
        const url = '/user/mobile/otp';
        $http.post(url).then(function (output) {
            callback(output);
        })
    }

    factory.verifyMobileOtp = function (otp, callback) {
        const url = '/user/mobile/verify';
        var request = {
            otp: otp
        }
        $http.post(url, request).then(function (output) {
            callback(output);
        })
    }

    factory.addRecord = function (record, callback) {
        const url = '/user/record';
        $http.post(url, record).then(function (output) {
            callback(output);
        })
    };

    factory.updateRecord = function (record, callback) {
        const url = '/user/record/update';
        $http.post(url, record).then(function (output) {
            callback(output);
        })
    }

    factory.submitSkills = function (skills, callback) {
        const url = '/user/skills/update';
        $http.post(url, skills).then(function (output) {
            callback(output);
        });
    };

    factory.getRequests = function (callback) {
        const url = '/user/requests/all';
        $http.get(url).then(function (output) {
            callback(output);
        });
    };

    factory.logout = function (callback) {
        const url = '/user/logout';
        $http.get(url).then(function (output) {
            callback(output);
        });
    };

    factory.requestDocument = function (request, callback) {
        const url = '/user/record/document/request';
        $http.post(url, request).then(function (output) {
            callback(output);
        });
    };

    factory.signDocument = function (request, callback) {
        const url = '/user/record/document/sign';
        $http.post(url, request).then(function (output) {
            callback(output);
        });
    };

    factory.handleAccess = function (request, callback) {
        const url = '/user/record/document/request/handle'
        $http.post(url, request).then(function (output) {
            callback(output);
        })
    }

    factory.getFromIpfs = function (multihash, callback) {
        const url = '/ipfs/' + multihash;
        $http.get(url).then(function (output) {
            callback(output);
        })
    }

    factory.userInformed = function (callback) {
        const url = '/user/informed';
        $http.post(url).then(function (output) {
            callback(output);
        })
    }

    factory.verifyPassword = function (password, callback) {
        const url = '/user/password';
        $http.post(url, password).then(function (output) {
            callback(output);
        })
    }

    factory.profileIntroduced = function (callback) {
        const url = '/user/profileIntro';
        $http.post(url).then(function (output) {
            callback(output);
        })
    }

    factory.requestIntroduced = function (callback) {
        const url = '/user/requestIntro';
        $http.post(url).then(function (output) {
            callback(output);
        })
    }

    factory.walletIntroduced = function (callback) {
        const url = '/user/walletIntro';
        $http.post(url).then(function (output) {
            callback(output);
        })
    }

    factory.paymentTest = function(callback) {
        const  url = '/user/payment';
        $http.post(url).then(function(output) {
            callback(output);
        })
    }

    return factory;

})